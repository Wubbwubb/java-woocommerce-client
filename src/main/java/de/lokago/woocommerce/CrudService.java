package de.lokago.woocommerce;

public interface CrudService<T extends CrudObject<ID>, ID, C> {
	T create(T object, C context) throws IllegalArgumentException;
	T read(ID key, C context) throws IllegalArgumentException;
	T update(T object, C context) throws IllegalArgumentException;
	T delete(T object, C context) throws IllegalArgumentException;
}