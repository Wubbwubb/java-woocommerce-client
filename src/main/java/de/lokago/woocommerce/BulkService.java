package de.lokago.woocommerce;

import de.lokago.woocommerce.jersey.FilterQuery;

import java.util.List;
import java.util.Map;

public interface BulkService<T extends CrudObject<ID>, ID, C> {
	List<T> write(List<T> items, C context);
	List<T> read(C context);
	List<T> read(C context, FilterQuery filter, Map<String, String> queryParams);
	Integer count(C context, FilterQuery filter, Map<String, String> queryParams);
}
