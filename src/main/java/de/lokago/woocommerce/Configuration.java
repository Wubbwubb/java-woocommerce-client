package de.lokago.woocommerce;

public interface Configuration {

	String getTargetUrl();
	String getBasePath();
	String getConsumerKey();
	String getConsumerSecret();
}
