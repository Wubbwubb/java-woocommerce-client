package de.lokago.woocommerce;

public interface CrudObject<ID> {

	ID getId();
	
}