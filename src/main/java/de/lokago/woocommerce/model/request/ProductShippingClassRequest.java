package de.lokago.woocommerce.model.request;

import de.lokago.woocommerce.model.ProductShippingClass;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class ProductShippingClassRequest {

	private ProductShippingClass productShippingClass;
	
	public ProductShippingClassRequest() {}
	
	public ProductShippingClassRequest(ProductShippingClass productShippingClass) {
		this.productShippingClass = productShippingClass;
	}

	@XmlElement(name="product_shipping_class")
	public ProductShippingClass getProductShippingClass() {
		return productShippingClass;
	}

	public void setProductShippingClass(ProductShippingClass productShippingClass) {
		this.productShippingClass = productShippingClass;
	}
	
}
