package de.lokago.woocommerce.model.request;

import de.lokago.woocommerce.model.Customer;

import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

@XmlRootElement
public class CustomersRequest {
	
	public static final String FILTER_ROLE_KEY = "role";
	public static final String FILTER_ROLE_CUSTOMER_VAL = "customer";
	public static final String FILTER_ROLE_SUBSCRIBER_VAL = "subscriber";
	
	private List<Customer> customers;

	public CustomersRequest(List<Customer> items) {
		this.customers = items;
	}

	public List<Customer> getCustomers() {
		return customers;
	}

	public void setCustomers(List<Customer> customers) {
		this.customers = customers;
	}

}
