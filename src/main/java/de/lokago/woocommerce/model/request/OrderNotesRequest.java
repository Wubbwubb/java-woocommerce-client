package de.lokago.woocommerce.model.request;

import de.lokago.woocommerce.model.OrderNote;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.ArrayList;
import java.util.List;

@XmlRootElement
public class OrderNotesRequest {

	private List<OrderNote> orderNotes;
	
	public OrderNotesRequest() {
		this.orderNotes = new ArrayList<OrderNote>();
	}
	
	public OrderNotesRequest(List<OrderNote> items) {
		this.orderNotes = items;
	}

	@XmlElement(name="order_notes")
	public List<OrderNote> getOrderNotes() {
		return orderNotes;
	}

	public void setOrderNotes(List<OrderNote> orderNotes) {
		this.orderNotes = orderNotes;
	}
	
}
