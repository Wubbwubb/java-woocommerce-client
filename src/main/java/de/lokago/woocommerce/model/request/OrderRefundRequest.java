package de.lokago.woocommerce.model.request;

import de.lokago.woocommerce.model.OrderRefund;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class OrderRefundRequest {

	private OrderRefund orderRefund;
	
	public OrderRefundRequest() {}
	
	public OrderRefundRequest(OrderRefund orderRefund) {
		this.orderRefund = orderRefund;
	}

	@XmlElement(name="order_refund")
	public OrderRefund getOrderRefund() {
		return orderRefund;
	}

	public void setOrderRefund(OrderRefund orderRefund) {
		this.orderRefund = orderRefund;
	}
	
}
