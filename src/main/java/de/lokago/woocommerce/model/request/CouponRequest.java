package de.lokago.woocommerce.model.request;

import de.lokago.woocommerce.model.Coupon;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class CouponRequest {
	
	private Coupon coupon;
	
	public CouponRequest() {}
	
	public CouponRequest(Coupon coupon) {
		this.coupon = coupon;
	}

	public Coupon getCoupon() {
		return coupon;
	}

	public void setCoupon(Coupon coupon) {
		this.coupon = coupon;
	}
}
