package de.lokago.woocommerce.model.request;

import de.lokago.woocommerce.model.OrderNote;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class OrderNoteRequest {

	private OrderNote orderNote;
	
	public OrderNoteRequest() {}
	
	public OrderNoteRequest(OrderNote orderNote) {
		this.orderNote = orderNote;
	}

	@XmlElement(name="order_note")
	public OrderNote getOrderNote() {
		return orderNote;
	}

	public void setOrderNote(OrderNote orderNote) {
		this.orderNote = orderNote;
	}
	
}
