package de.lokago.woocommerce.model.request;

import de.lokago.woocommerce.model.ProductAttributeTerm;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class ProductAttributeTermRequest {

	private ProductAttributeTerm productAttributeTerm;
	
	public ProductAttributeTermRequest() {}

	public ProductAttributeTermRequest(ProductAttributeTerm object) {
		this.productAttributeTerm = object;
	}

	@XmlElement(name="product_attribute_term")
	public ProductAttributeTerm getProductAttributeTerm() {
		return productAttributeTerm;
	}

	public void setProductAttributeTerm(ProductAttributeTerm productAttributeTerm) {
		this.productAttributeTerm = productAttributeTerm;
	}

}
