package de.lokago.woocommerce.model.request;

import de.lokago.woocommerce.model.Customer;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class CustomerRequest {

	private Customer customer;
	
	public CustomerRequest() {}
	
	public CustomerRequest(Customer customer) {
		this.customer = customer;
	}

	public Customer getCustomer() {
		return customer;
	}

	public void setCustomer(Customer customer) {
		this.customer = customer;
	}
}
