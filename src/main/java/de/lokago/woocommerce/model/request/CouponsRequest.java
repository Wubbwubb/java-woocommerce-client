package de.lokago.woocommerce.model.request;

import de.lokago.woocommerce.model.Coupon;

import javax.xml.bind.annotation.XmlRootElement;
import java.util.ArrayList;
import java.util.List;

@XmlRootElement
public class CouponsRequest {

	private List<Coupon> coupons;
	
	public CouponsRequest() {
		this.coupons = new ArrayList<Coupon>();
	}
	
	public CouponsRequest(List<Coupon> coupons) {
		this.coupons = coupons;
	}
	
	public List<Coupon> getCoupons() {
		return coupons;
	}

	public void setCoupons(List<Coupon> coupons) {
		this.coupons = coupons;
	}

}
