package de.lokago.woocommerce.model.request;

import de.lokago.woocommerce.model.ProductAttribute;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class ProductAttributeRequest {

	private ProductAttribute productAttribute;
	
	public ProductAttributeRequest() {}
	
	public ProductAttributeRequest(ProductAttribute productAttribute) {
		this.productAttribute = productAttribute;
	}

	@XmlElement(name="product_attribute")
	public ProductAttribute getProductAttribute() {
		return productAttribute;
	}

	public void setProductAttribute(ProductAttribute productAttribute) {
		this.productAttribute = productAttribute;
	}
	
}
