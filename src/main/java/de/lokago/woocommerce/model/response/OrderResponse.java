package de.lokago.woocommerce.model.response;

import de.lokago.woocommerce.model.Order;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class OrderResponse extends NetworkResponse {
	
	private Order order;
	
	public OrderResponse() {}
	
	public OrderResponse(Order order) {
		this.order = order;
	}

	public Order getOrder() {
		return order;
	}

	public void setOrder(Order order) {
		this.order = order;
	}
	
}
