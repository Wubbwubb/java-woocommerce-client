package de.lokago.woocommerce.model.response;

import de.lokago.woocommerce.model.Customer;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class CustomerResponse extends NetworkResponse {

	private Customer customer;
	
	public CustomerResponse() {}
	
	public CustomerResponse(Customer customer) {
		this.customer = customer;
	}

	public Customer getCustomer() {
		return customer;
	}

	public void setCustomer(Customer customer) {
		this.customer = customer;
	}
}
