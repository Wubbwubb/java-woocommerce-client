package de.lokago.woocommerce.model.response;

import de.lokago.woocommerce.model.OrderNote;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class OrderNoteResponse extends NetworkResponse {

	private OrderNote orderNote;
	
	public OrderNoteResponse() {}
	
	public OrderNoteResponse(OrderNote orderNote) {
		this.orderNote = orderNote;
	}

	@XmlElement(name="order_note")
	public OrderNote getOrderNote() {
		return orderNote;
	}

	public void setOrderNote(OrderNote orderNote) {
		this.orderNote = orderNote;
	}
	
}
