package de.lokago.woocommerce.model.response;

import de.lokago.woocommerce.model.Coupon;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class CouponResponse extends NetworkResponse {
	
	private Coupon coupon;
	
	public CouponResponse() {}
	
	public CouponResponse(Coupon coupon) {
		this.coupon = coupon;
	}

	public Coupon getCoupon() {
		return coupon;
	}

	public void setCoupon(Coupon coupon) {
		this.coupon = coupon;
	}
}
