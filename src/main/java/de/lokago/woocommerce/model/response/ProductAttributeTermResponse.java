package de.lokago.woocommerce.model.response;

import de.lokago.woocommerce.model.ProductAttributeTerm;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class ProductAttributeTermResponse extends NetworkResponse {

	private ProductAttributeTerm productAttributeTerm; 
	
	public ProductAttributeTermResponse() {}

	@XmlElement(name="product_attribute_term")
	public ProductAttributeTerm getProductAttributeTerm() {
		return productAttributeTerm;
	}

	public void setProductAttributeTerm(ProductAttributeTerm productAttributeTerm) {
		this.productAttributeTerm = productAttributeTerm;
	}

}
