package de.lokago.woocommerce.model.response;

import de.lokago.woocommerce.model.ProductCategory;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class ProductCategoryResponse extends NetworkResponse {

	private ProductCategory productCategory;
	
	public ProductCategoryResponse() {}

	@XmlElement(name="product_category")
	public ProductCategory getProductCategory() {
		return productCategory;
	}

	public void setProductCategory(ProductCategory productCategory) {
		this.productCategory = productCategory;
	}
	
	
}
