package de.lokago.woocommerce.model.response;

import de.lokago.woocommerce.model.OrderRefund;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class OrderRefundResponse extends NetworkResponse {

	private OrderRefund orderRefund;
	
	public OrderRefundResponse() {}
	
	public OrderRefundResponse(OrderRefund orderRefund) {
		this.orderRefund = orderRefund;
	}

	@XmlElement(name="order_refund")
	public OrderRefund getOrderRefund() {
		return orderRefund;
	}

	public void setOrderRefund(OrderRefund orderRefund) {
		this.orderRefund = orderRefund;
	}
	
}
