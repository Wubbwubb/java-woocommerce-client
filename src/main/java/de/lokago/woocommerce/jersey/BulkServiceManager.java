package de.lokago.woocommerce.jersey;

import de.lokago.woocommerce.BulkService;
import de.lokago.woocommerce.Configuration;
import de.lokago.woocommerce.jersey.bulk.*;
import de.lokago.woocommerce.model.*;

public class BulkServiceManager {

	public static BulkService<Coupon, Integer, Object> createCouponBulkService(Configuration config) {
		return new CouponBulkService(CrudServiceManager.getWebTarget(config));
	}
	
	public static BulkService<Customer, Integer, Object> createCustomerBulkService(Configuration config) {
		return new CustomerBulkService(CrudServiceManager.getWebTarget(config));
	}
		
	public static BulkService<Order, Integer, Object> createOrderBulkService(Configuration config) {
		return new OrderBulkService(CrudServiceManager.getWebTarget(config));
	}
	
	public static BulkService<OrderNote, Integer, Order> createOrderNoteBulkService(Configuration config) {
		return new OrderNoteBulkService(CrudServiceManager.getWebTarget(config));
	}
	
	public static BulkService<OrderRefund, Integer, Order> createOrderRefundsBulkService(Configuration config) {
		return new OrderRefundBulkService(CrudServiceManager.getWebTarget(config));
	}
	
	public static BulkService<ProductAttribute, Integer, Object> createProductAttributeBulkService(Configuration config) {
		return new ProductAttributeBulkService(CrudServiceManager.getWebTarget(config));
	}
	
	public static BulkService<ProductAttributeTerm, Integer, ProductAttribute> createProductAttributeTermBulkService(Configuration config) {
		return new ProductAttributeTermBulkService(CrudServiceManager.getWebTarget(config));
	}
	
	public static BulkService<Product, Integer, Object> createProductBulkService(Configuration config) {
		return new ProductBulkService(CrudServiceManager.getWebTarget(config));
	}
	
	public static BulkService<ProductCategory, Integer, Object> createProductCategoryBulkService(Configuration config) {
		return new ProductCategoryBulkService(CrudServiceManager.getWebTarget(config));
	}
	
	public static BulkService<Order, Integer, Product> createProductOrderBulkService(Configuration config) {
		return new ProductOrderBulkService(CrudServiceManager.getWebTarget(config));
	}
	
	public static BulkService<ProductReview, Integer, Product> createProductReviewBulkService(Configuration config) {
		return new ProductReviewBulkService(CrudServiceManager.getWebTarget(config));
	}
	
	public static BulkService<ProductShippingClass, Integer, Object> createProductShippingClassBulkService(Configuration config) {
		return new ProductShippingClassBulkService(CrudServiceManager.getWebTarget(config));
	}
	
	public static BulkService<ProductTag, Integer, Object> createProductTagBulkService(Configuration config) {
		return new ProductTagBulkService(CrudServiceManager.getWebTarget(config));
	}
}
