package de.lokago.woocommerce.jersey.bulk;

import de.lokago.woocommerce.model.ProductCategory;

import javax.ws.rs.client.WebTarget;
import java.util.Arrays;
import java.util.List;

public class ProductCategoryBulkService extends TemplateReadOnlyBulkService<ProductCategory, Integer, Object, ProductCategory[]> {

	public static final String READ_BASE_PATH = "products/categories";
	
	public ProductCategoryBulkService(WebTarget target) {
		super(target);
	}

	@Override
	public String getReadPath(Object context) {
		return READ_BASE_PATH;
	}

	@Override
	public Class<ProductCategory[]> getResponseClass() {
		return ProductCategory[].class;
	}

	@Override
	public List<ProductCategory> getResult(ProductCategory[] productCategories) {
		return Arrays.asList(productCategories);
	}

}
