package de.lokago.woocommerce.jersey.bulk;

import de.lokago.woocommerce.model.Order;
import de.lokago.woocommerce.model.OrderNote;

import javax.ws.rs.client.WebTarget;
import java.util.Arrays;
import java.util.List;

public class OrderNoteBulkService extends TemplateReadOnlyBulkService<OrderNote, Integer, Order, OrderNote[]> {

	public static final String READ_BASE_PATH = "orders/%d/notes";
	
	public OrderNoteBulkService(WebTarget target) {
		super(target);
	}

	@Override
	public String getReadPath(Order context) {
		return String.format(READ_BASE_PATH, context.getId());
	}

	@Override
	public Class<OrderNote[]> getResponseClass() {
		return OrderNote[].class;
	}

	@Override
	public List<OrderNote> getResult(OrderNote[] orderNotes) {
		return Arrays.asList(orderNotes);
	}

}
