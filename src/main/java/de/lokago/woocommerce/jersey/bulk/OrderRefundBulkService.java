package de.lokago.woocommerce.jersey.bulk;

import de.lokago.woocommerce.model.Order;
import de.lokago.woocommerce.model.OrderRefund;

import javax.ws.rs.client.WebTarget;
import java.util.Arrays;
import java.util.List;

public class OrderRefundBulkService extends TemplateReadOnlyBulkService<OrderRefund, Integer, Order, OrderRefund[]> {

	public static final String READ_BASE_PATH = "orders/%d/refunds";
	
	public OrderRefundBulkService(WebTarget target) {
		super(target);
	}

	@Override
	public String getReadPath(Order context) {
		return String.format(READ_BASE_PATH, context.getId());
	}

	@Override
	public Class<OrderRefund[]> getResponseClass() {
		return OrderRefund[].class;
	}

	@Override
	public List<OrderRefund> getResult(OrderRefund[] orderRefunds) {
		return Arrays.asList(orderRefunds);
	}

}
