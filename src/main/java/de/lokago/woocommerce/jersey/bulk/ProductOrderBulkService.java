package de.lokago.woocommerce.jersey.bulk;

import de.lokago.woocommerce.model.Order;
import de.lokago.woocommerce.model.Product;

import javax.ws.rs.client.WebTarget;
import java.util.Arrays;
import java.util.List;

public class ProductOrderBulkService extends TemplateReadOnlyBulkService<Order, Integer, Product, Order[]> {

	public static final String READ_BASE_PATH = "products/%d/orders";
	
	public ProductOrderBulkService(WebTarget target) {
		super(target);
	}

	@Override
	public String getReadPath(Product context) {
		return String.format(READ_BASE_PATH, context.getId());
	}

	@Override
	public Class<Order[]> getResponseClass() {
		return Order[].class;
	}

	@Override
	public List<Order> getResult(Order[] orders) {
		return Arrays.asList(orders);
	}

}
