package de.lokago.woocommerce.jersey.crud;

import de.lokago.woocommerce.model.ProductAttribute;
import de.lokago.woocommerce.model.request.ProductAttributeRequest;

import javax.ws.rs.client.WebTarget;

public class ProductAttributeCrudService extends TemplateCrudService<ProductAttribute, Integer, Object, ProductAttributeRequest, ProductAttribute> {

	public static final String BASE_PATH = "products/attributes";
	public static final String BASE_PATH_FORMAT = "products/attributes/%d";
	
	public ProductAttributeCrudService(WebTarget target) {
		super(target);
	}

	@Override
	public String getCreatePath(Object context) {
		return BASE_PATH;
	}

	@Override
	public String getReadPath(Integer id, Object context) {
		return String.format(BASE_PATH_FORMAT, id);
	}

	@Override
	public String getUpdatePath(Integer id, Object context) {
		return String.format(BASE_PATH_FORMAT, id);
	}

	@Override
	public String getDeletePath(Integer id, Object context) {
		return String.format(BASE_PATH_FORMAT, id);
	}

	@Override
	public Class<ProductAttribute> getResponseClass() {
		return ProductAttribute.class;
	}

	@Override
	public ProductAttributeRequest getRequest(ProductAttribute object) {
		return new ProductAttributeRequest(object);
	}

	@Override
	public ProductAttribute getResult(ProductAttribute network) {
		return network;
	}

}
