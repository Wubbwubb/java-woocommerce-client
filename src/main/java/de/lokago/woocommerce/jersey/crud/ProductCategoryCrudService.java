package de.lokago.woocommerce.jersey.crud;

import de.lokago.woocommerce.model.ProductCategory;
import de.lokago.woocommerce.model.request.ProductCategoryRequest;

import javax.ws.rs.client.WebTarget;

public class ProductCategoryCrudService extends TemplateCrudService<ProductCategory, Integer, Object, ProductCategoryRequest, ProductCategory> {

	public static final String BASE_PATH = "products/categories";
	public static final String BASE_PATH_FORMAT = "products/categories/%d";
	
	public ProductCategoryCrudService(WebTarget target) {
		super(target);
	}

	@Override
	public String getCreatePath(Object context) {
		return BASE_PATH;
	}

	@Override
	public String getReadPath(Integer id, Object context) {
		return String.format(BASE_PATH_FORMAT, id);
	}

	@Override
	public String getUpdatePath(Integer id, Object context) {
		return String.format(BASE_PATH_FORMAT, id);
	}

	@Override
	public String getDeletePath(Integer id, Object context) {
		return String.format(BASE_PATH_FORMAT, id);
	}

	@Override
	public Class<ProductCategory> getResponseClass() {
		return ProductCategory.class;
	}

	@Override
	public ProductCategoryRequest getRequest(ProductCategory object) {
		return new ProductCategoryRequest(object);
	}

	@Override
	public ProductCategory getResult(ProductCategory network) {
		return network;
	}

}
