package de.lokago.woocommerce.jersey.crud;

import de.lokago.woocommerce.model.ProductShippingClass;
import de.lokago.woocommerce.model.request.ProductShippingClassRequest;

import javax.ws.rs.client.WebTarget;

public class ProductShippingClassCrudService extends TemplateCrudService<ProductShippingClass, Integer, Object, ProductShippingClassRequest, ProductShippingClass> {

	public static final String BASE_PATH = "products/shipping_classes";
	public static final String BASE_PATH_FORMAT = "products/shipping_classes/%d";	
	
	public ProductShippingClassCrudService(WebTarget target) {
		super(target);
	}

	@Override
	public String getCreatePath(Object context) {
		return BASE_PATH;
	}

	@Override
	public String getReadPath(Integer id, Object context) {
		return String.format(BASE_PATH_FORMAT, id);
	}

	@Override
	public String getUpdatePath(Integer id, Object context) {
		return String.format(BASE_PATH_FORMAT, id);
	}

	@Override
	public String getDeletePath(Integer id, Object context) {
		return String.format(BASE_PATH_FORMAT, id);
	}

	@Override
	public Class<ProductShippingClass> getResponseClass() {
		return ProductShippingClass.class;
	}

	@Override
	public ProductShippingClassRequest getRequest(ProductShippingClass object) {
		return new ProductShippingClassRequest(object);
	}

	@Override
	public ProductShippingClass getResult(ProductShippingClass network) {
		return network;
	}

}
