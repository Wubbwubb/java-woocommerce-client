package de.lokago.woocommerce.jersey.crud;

import de.lokago.woocommerce.model.Order;
import de.lokago.woocommerce.model.OrderRefund;
import de.lokago.woocommerce.model.request.OrderRefundRequest;

import javax.ws.rs.client.WebTarget;

public class OrderRefundCrudService extends TemplateCrudService<OrderRefund, Integer, Order, OrderRefundRequest, OrderRefund> {
	
	public static final String BASE_PATH = OrderCrudService.BASE_PATH_FORMAT + "/refunds";
	public static final String BASE_PATH_FORMAT = OrderCrudService.BASE_PATH_FORMAT + "/refunds/%d";
	
	public OrderRefundCrudService(WebTarget target) {
		super(target);
	}

	@Override
	public String getCreatePath(Order context) {
		return String.format(BASE_PATH, context.getId());
	}

	@Override
	public String getReadPath(Integer id, Order context) {
		return String.format(BASE_PATH_FORMAT, context.getId(), id);
	}

	@Override
	public String getUpdatePath(Integer id, Order context) {
		return String.format(BASE_PATH_FORMAT, context.getId(), id);
	}

	@Override
	public String getDeletePath(Integer id, Order context) {
		return String.format(BASE_PATH_FORMAT, context.getId(), id);
	}

	@Override
	public Class<OrderRefund> getResponseClass() {
		return OrderRefund.class;
	}

	@Override
	public OrderRefundRequest getRequest(OrderRefund object) {
		return new OrderRefundRequest(object);
	}

	@Override
	public OrderRefund getResult(OrderRefund network) {
		return network;
	}

}
