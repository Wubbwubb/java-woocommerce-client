package de.lokago.woocommerce.jersey.crud;

import de.lokago.woocommerce.model.Order;
import de.lokago.woocommerce.model.OrderNote;
import de.lokago.woocommerce.model.request.OrderNoteRequest;

import javax.ws.rs.client.WebTarget;

public class OrderNoteCrudService extends TemplateCrudService<OrderNote, Integer, Order, OrderNoteRequest, OrderNote> {
	
	public static final String BASE_PATH = OrderCrudService.BASE_PATH_FORMAT + "/notes";
	public static final String BASE_PATH_FORMAT = OrderCrudService.BASE_PATH_FORMAT + "/notes/%d";
	
	public OrderNoteCrudService(WebTarget target) {
		super(target);
	}

	@Override
	public String getCreatePath(Order context) {
		return String.format(BASE_PATH, context.getId());
	}

	@Override
	public String getReadPath(Integer id, Order context) {
		return String.format(BASE_PATH_FORMAT, context.getId(), id);
	}

	@Override
	public String getUpdatePath(Integer id, Order context) {
		return String.format(BASE_PATH_FORMAT, context.getId(), id);
	}

	@Override
	public String getDeletePath(Integer id, Order context) {
		return String.format(BASE_PATH_FORMAT, context.getId(), id);
	}

	@Override
	public Class<OrderNote> getResponseClass() {
		return OrderNote.class;
	}

	@Override
	public OrderNoteRequest getRequest(OrderNote object) {
		return new OrderNoteRequest(object);
	}

	@Override
	public OrderNote getResult(OrderNote response) {
		return response;
	}

}
